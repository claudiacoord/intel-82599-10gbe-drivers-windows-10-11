### Установщик оригинальных драйверов Intel для NIC карт на чипсете 82599 в Windows 10/11 x64.

IIntel прекратила поддержку новых драйверов для чипсета 82599 под Windows 10/11, но предыдущие выпуски драйверов прекрасно продолжают работать. Установщик от Intel умышленно игнорирует обновление драйверов установленных карт на чипсете 82599. Не без участия компании Microsoft...

**<mark>ПОМНИТЕ, ВЫ УСТАНАВЛИВАЕТЕ 'УСТАРЕВШИЕ' ДРАЙВЕРА, И ВСЕ РИСКИ, А ТАКЖЕ, РАБОТОСПОСОБНОСТЬ ИЛИ ПОВРЕЖДЕНИЕ ОБОРУДОВАНИЯ И ДРУГИЕ РИСКИ ПОЛНОСТЬЮ ЛЕЖАТ НА ВАШЕЙ ОТВЕТСТВЕННОСТИ!</mark>**

Файлы пакета драйверов в этом установщике можно использовать для установки драйверов для перечисленных ниже адаптеров.


<u>СПИСОК КАРТ И НАЗВАНИЯ ДРАЙВЕРОВ, ВКЛЮЧЕННЫЕ В ПАКЕТ</u>:

* **Драйвер ixn68x64 (4.1.239.0 - 03/13/2021)**:
  
  * Intel(R) 10 Gigabit BR KX4 Dual Port Network Connection
  * Intel(R) 82599 10 Gigabit Network Connection
  * Intel(R) 82599 Multi-Function Network Device
  * Intel(R) 82599 10 Gigabit Dual Port Network Connection
  * Intel(R) 82599 10 Gigabit Dual Port Backplane Connection
  * Intel(R) 82599 10 Gigabit CX4 Dual Port Network Connection
  * Intel(R) 82599 10 Gigabit TN Network Connection
  * Intel(R) Ethernet Mezzanine Adapter X520-KX4-2
  * Intel(R) Ethernet X520 10GbE Dual Port KX4 Mezz
  * Intel(R) Ethernet X520 10GbE Dual Port KX4-KR Mezz
  * Intel(R) Ethernet 10G 2P X520-k bNDC
  * Intel(R) Ethernet ExpressModule X520-P2
  * Intel(R) Ethernet 10G 2P X520 Adapter
  * Intel(R) Ethernet OCP Server Adapter X520-2
  * Intel(R) Ethernet OCP Server Adapter X520-1 
  * Intel(R) Ethernet Server Adapter X520-1
  * Intel(R) Ethernet Server Adapter X520-2
  * Intel(R) Ethernet Server Adapter X520-DA2 
  * Intel(R) Ethernet 10G 4P X520/I350 rNDC
  * Intel(R) Ethernet 10G X520 LOM
  * Intel(R) Ethernet 10GSFP+ DP Embedded CNA X520-2 
  * Intel(R) Ethernet Server Adapter X520-T2 
  * Intel(R) Ethernet Converged Network Adapter X520-4
  * Intel(R) Ethernet Converged Network Adapter X520-Q1
  * Sun Dual 10GbE PCIe 2.0 FEM
  * Sun Dual 10GBASE-T LP
  * Lenovo ThinkServer X520-2 AnyFabric
  * Cisco UCS CNA M61KR-I Intel Converged Network Adapter
  * HPE Ethernet 10Gb 2-port 560SFP+ Adapter
  * HPE Ethernet 10Gb 2-port 560FLB Adapter
  * HPE Ethernet 10Gb 2-port 560M Adapter
  * HPE Ethernet 10Gb 2-port 560FLR-SFP+ Adapter
  * HPE Ethernet 10Gb 1-port P560FLR-SFP+ Adapter
  * HPE Ethernet 10Gb 2-port 562i Adapter

* **Драйвер ixs68x64 (4.1.239.0 - 03/13/2021)**:
  
  * Intel(R) X550 Multi-Function Network Device
  * Intel(R) Ethernet Controller X550
  * Intel(R) Ethernet 10G 2P X550-t Adapter
  * Intel(R) Ethernet 10G 4P X550 rNDC
  * Intel(R) Ethernet 10G 4P X550/I350 rNDC 
  * Intel(R) Ethernet Converged Network Adapter X550-T2
  * Intel(R) Ethernet Converged Network Adapter X550-T1
  * Intel(R) Ethernet Server Adapter X550-T2 for OCP
  * Intel(R) Ethernet Server Adapter X550-T1 for OCP
  * Cisco X550-TX 10 Gig LOM
  * Cisco(R) Ethernet Converged NIC X550-T2
  * HPE Ethernet 10Gb 2-port 562T Adapter
  * HPE Ethernet 10Gb 2-port 562FLR-T Adapter

* **Driver sxb68x64 (4.1.239.0 - 03/13/2021)**:
  
  * Intel(R) X552 Multi-Function Network Device
  * Intel(R) Ethernet Connection X552 10 GbE Backplane 
  * Intel(R) Ethernet Connection X552 10 GbE SFP+
  * Intel(R) Ethernet Connection X552/X557-AT 10GBASE-T
  * Intel(R) Ethernet Connection X552 1000BASE-T

* **Драйвер sxa68x64 (4.1.243.0 - 06/22/2021)**:
  
  * Intel(R) X553 Multi-Function Network Device
  * Intel(R) Ethernet Connection X553 Backplane 
  * Intel(R) Ethernet Connection X553 10 GbE SFP+
  * Intel(R) Ethernet Connection X553/X557-AT 10GBASE-T
  * Intel(R) Ethernet Connection X553 1GbE

* **Драйвер ixt68x64 (4.1.228.0 - 11/25/2020)**:
  
  * Intel(R) X540 Multi-Function Network Device
  * Intel(R) Ethernet 10G 2P X540-t Adapter
  * Intel(R) Ethernet Converged Network Adapter X540-T2
  * Intel(R) Ethernet Converged Network Adapter X540-T1
  * Intel(R) Ethernet Controller X540
  * Intel(R) Ethernet Controller X540-AT2
  * Intel(R) Ethernet 10G 4P X540/I350 rNDC
  * Intel(R) Ethernet 10GBT DP Embedded CNA X540-T2
  * Cisco X540-TX 10 Gig LOM
  * Lenovo ThinkServer X540-T2 AnyFabric
  * Sun Dual Port 10 GbE PCIe 2.0 Low Profile Adapter, Base-T
  * Sun Dual Port 10 GbE PCIe 2.0 ExpressModule, Base-T
  * HPE Ethernet 10Gb 2-port 561FLR-T Adapter
  * HPE Ethernet 10Gb 2-port 561T Adapter

* **Драйвер vxn68x64 (2.1.241.0 - 09/09/2021)**:
  
  * Intel(R) 82599 Virtual Network Function
  * Intel(R) X540 Virtual Network Function

* **Драйвер vxs68x64 (2.1.230.0 - 01/05/2021)**:
  
  * Intel(R) X550 Virtual Network Function
  * Intel(R) X552 Virtual Network Function
  * Intel(R) X553 Virtual Network Function


<u>СПИСОК ПОДДЕРЖИВАЕМЫХ «**PCI\IDS**» ДЛЯ КАЖДОГО ТИПА ДРАЙВЕРА</u>:

* **Драйвер ixn68x64 (4.1.239.0 - 03/13/2021)**:
  
  * PCI\VEN_8086&DEV_10F7
  * PCI\VEN_8086&DEV_10F8
  * PCI\VEN_8086&DEV_10F9
  * PCI\VEN_8086&DEV_10FB
  * PCI\VEN_8086&DEV_10FC
  * PCI\VEN_8086&DEV_152A
  * PCI\VEN_8086&DEV_1529
  * PCI\VEN_8086&DEV_1557
  * PCI\VEN_8086&DEV_151C
  * PCI\VEN_8086&DEV_10F8&SUBSYS_000C8086
  * PCI\VEN_8086&DEV_10F7&SUBSYS_000D8086
  * PCI\VEN_8086&DEV_10F7&SUBSYS_7B12108E
  * PCI\VEN_8086&DEV_1514&SUBSYS_000B8086
  * PCI\VEN_8086&DEV_10F8&SUBSYS_1F631028
  * PCI\VEN_8086&DEV_10FB&SUBSYS_7B11108E
  * PCI\VEN_8086&DEV_1507&SUBSYS_7B10108E
  * PCI\VEN_8086&DEV_1517&SUBSYS_006A1137
  * PCI\VEN_8086&DEV_10F8&SUBSYS_18D0103C
  * PCI\VEN_8086&DEV_10F8&SUBSYS_17D2103C
  * PCI\VEN_8086&DEV_10FB&SUBSYS_17D0103C
  * PCI\VEN_8086&DEV_10FB&SUBSYS_211B103C
  * PCI\VEN_8086&DEV_10FB&SUBSYS_2159103C
  * PCI\VEN_8086&DEV_10FB&SUBSYS_00088086
  * PCI\VEN_8086&DEV_1557&SUBSYS_00018086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_107117AA
  * PCI\VEN_8086&DEV_10FB&SUBSYS_17D3103C
  * PCI\VEN_8086&DEV_10FB&SUBSYS_00028086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_000A8086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_00068086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_00038086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_000C8086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_7A128086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_11A91734
  * PCI\VEN_8086&DEV_10FB&SUBSYS_7A118086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_8975152D
  * PCI\VEN_8086&DEV_154D&SUBSYS_7B118086
  * PCI\VEN_8086&DEV_10FB&SUBSYS_1F721028
  * PCI\VEN_8086&DEV_10FB&SUBSYS_06EE1028
  * PCI\VEN_8086&DEV_10FB&SUBSYS_04708086
  * PCI\VEN_8086&DEV_151C&SUBSYS_A02C8086
  * PCI\VEN_8086&DEV_151C&SUBSYS_A21C8086
  * PCI\VEN_8086&DEV_151C&SUBSYS_A03C8086
  * PCI\VEN_8086&DEV_151C&SUBSYS_7B13108E
  * PCI\VEN_8086&DEV_154A&SUBSYS_011A8086
  * PCI\VEN_8086&DEV_154A&SUBSYS_011B8086
  * PCI\VEN_8086&DEV_154A&SUBSYS_011C8086
  * PCI\VEN_8086&DEV_1558&SUBSYS_011A8086
  * PCI\VEN_8086&DEV_1558&SUBSYS_011B8086

* **Драйвер ixs68x64 (4.1.239.0 - 03/13/2021)**:
  
  * PCI\VEN_8086&DEV_1563
  * PCI\VEN_8086&DEV_15D1
  * PCI\VEN_8086&DEV_1563&SUBSYS_00018086
  * PCI\VEN_8086&DEV_1563&SUBSYS_001A8086
  * PCI\VEN_8086&DEV_15D1&SUBSYS_00028086
  * PCI\VEN_8086&DEV_1563&SUBSYS_00228086
  * PCI\VEN_8086&DEV_15D1&SUBSYS_00218086
  * PCI\VEN_8086&DEV_15D1&SUBSYS_00A28086
  * PCI\VEN_8086&DEV_1563&SUBSYS_001D8086
  * PCI\VEN_8086&DEV_1563&SUBSYS_1FA91028
  * PCI\VEN_8086&DEV_1563&SUBSYS_1FA81028
  * PCI\VEN_8086&DEV_1563&SUBSYS_00D11590
  * PCI\VEN_8086&DEV_1563&SUBSYS_00D21590
  * PCI\VEN_8086&DEV_1563&SUBSYS_01A21137
  * PCI\VEN_8086&DEV_1563&SUBSYS_01A31137
  * PCI\VEN_8086&DEV_1563&SUBSYS_01A41137
  * PCI\VEN_8086&DEV_1563&SUBSYS_01A51137
  * PCI\VEN_8086&DEV_1563&SUBSYS_02B21137
  * PCI\VEN_8086&DEV_1563&SUBSYS_02B31137
  * PCI\VEN_8086&DEV_1563&SUBSYS_001B8086
  * PCI\VEN_8086&DEV_15D1&SUBSYS_001B8086

* **Драйвер sxb68x64 (4.1.239.0 - 03/13/2021)**:
  
  * PCI\VEN_8086&DEV_15AA
  * PCI\VEN_8086&DEV_15AB
  * PCI\VEN_8086&DEV_15AC
  * PCI\VEN_8086&DEV_15AD
  * PCI\VEN_8086&DEV_15AE

* **Драйвер sxa68x64 (4.1.243.0 - 06/22/2021)**:
  
  * PCI\VEN_8086&DEV_15C2
  * PCI\VEN_8086&DEV_15C3
  * PCI\VEN_8086&DEV_15C4
  * PCI\VEN_8086&DEV_15CE
  * PCI\VEN_8086&DEV_15C8
  * PCI\VEN_8086&DEV_15E4
  * PCI\VEN_8086&DEV_15E5

* **Драйвер ixt68x64 (4.1.228.0 - 11/25/2020)**:
  
  * PCI\VEN_8086&DEV_1528
  * PCI\VEN_8086&DEV_1560
  * PCI\VEN_8086&DEV_1528&SUBSYS_00018086
  * PCI\VEN_8086&DEV_1528&SUBSYS_001A8086
  * PCI\VEN_8086&DEV_1528&SUBSYS_00BF1137
  * PCI\VEN_8086&DEV_1528&SUBSYS_50038086
  * PCI\VEN_8086&DEV_1528&SUBSYS_50048086
  * PCI\VEN_8086&DEV_1528&SUBSYS_00028086
  * PCI\VEN_8086&DEV_1528&SUBSYS_00A28086
  * PCI\VEN_8086&DEV_1528&SUBSYS_7B15108E
  * PCI\VEN_8086&DEV_1528&SUBSYS_00D41137
  * PCI\VEN_8086&DEV_1528&SUBSYS_7B14108E
  * PCI\VEN_8086&DEV_1528&SUBSYS_1F611028
  * PCI\VEN_8086&DEV_1528&SUBSYS_192D103C
  * PCI\VEN_8086&DEV_1528&SUBSYS_211A103C
  * PCI\VEN_8086&DEV_1528&SUBSYS_04718086
  * PCI\VEN_8086&DEV_1528&SUBSYS_107317AA

* **Драйвер vxn68x64 (2.1.241.0 - 09/09/2021)**:
  
  * PCI\VEN_8086&DEV_10ED
  * PCI\VEN_8086&DEV_1515
  * PCI\VEN_8086&DEV_152E 
  * PCI\VEN_8086&DEV_1530

* **Драйвер vxs68x64 (2.1.230.0 - 01/05/2021)**:
  
  * PCI\VEN_8086&DEV_1565
  * PCI\VEN_8086&DEV_15A8
  * PCI\VEN_8086&DEV_15C5
  * PCI\VEN_8086&DEV_1564
  * PCI\VEN_8086&DEV_15A9
  * PCI\VEN_8086&DEV_15B4


<u>**MIT License**</u>
<u>**2024 CC**</u>
