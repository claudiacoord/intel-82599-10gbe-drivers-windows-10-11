﻿
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; LicenseFile: "intel-drivers-en.rtf"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; LicenseFile: "intel-drivers-ru.rtf"

[CustomMessages]
IntelReadme=Intel devices readme file
russian.IntelReadme=Информация о выпуске от Intel
AppTitle=INTEL NIC 82599 drivers
russian.AppTitle=Драйверы INTEL NIC 82599
AppDescription=Installer of original Intel drivers for NIC cards on the 82599 chipsets in Windows 10/11 x64.
russian.AppDescription=Установщик оригинальных драйверов Intel для NIC карт на чипсете 82599 в Windows 10/11 x64.
AppProductName=Original Intel drivers for NIC cards on the 82599 chipsets.
russian.AppProductName=Оригинальные драйвера Intel для NIC карт на чипсете 82599.
InstallTitle=Installed
russian.InstallTitle=Устанавливается
DriverTitle=driver
russian.DriverTitle=драйвер
DriversTitle=Drivers
russian.DriversTitle=Драйверы
VDriverTitle=virtual driver
russian.VDriverTitle=виртуальный драйвер
VDriversTitle=Virtual drivers
russian.VDriversTitle=Виртуальные драйверы
ImReadme=I'll read the intel-82599-readme file for now...
russian.ImReadme=я пока почитаю файл intel-82599-readme

[Setup]
UsePreviousLanguage=no
AppName="{cm:AppTitle}"
AppVersion=4.1.239.0
VersionInfoDescription="{cm:AppDescription}"
VersionInfoProductName="{cm:AppProductName}"
OutputDir=..\Setup
OutputBaseFilename="INTEL-82599-driver-setup"
DefaultDirName={tmp}
DefaultGroupName="INTEL 82599 drivers"
Compression=lzma2
SolidCompression=yes
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64
SetupIconFile=intel-driver.ico
PrivilegesRequired=admin
MinVersion=10.0
DisableDirPage=yes
UsedUserAreasWarning=no

[Types]
Name: "IMREADME"; Description: "{cm:ImReadme}";
Name: "TTX520X560I350"; Description: "X520, 560FLB, 560FLR, 560M, 562i, I350, M61KR-I - (4.1.239.0)";
Name: "TTX550X562"; Description: "X550, 562T, 562FLR - (4.1.239.0)";
Name: "TTX540I350561T"; Description: "X540, I350, 561T, 561FLR - (4.1.228.0)";
Name: "TTX553"; Description: "X553 - (4.1.243.0)";
Name: "TTX552"; Description: "X552 - (4.1.239.0)";
Name: "TT82599X540VIRT"; Description: "82599, X540 - Virtual Function (2.1.241.0)";
Name: "TTX550X552X553VIRT"; Description: "X550, X552, X553 - Virtual Function (2.1.230.0)"; Flags: iscustom;

[Components]
Name: "INTELREADME"; Description: "{cm:IntelReadme}"; Types: TTX520X560I350 TTX550X562 TTX540I350561T TTX553 TTX552 TT82599X540VIRT TTX550X552X553VIRT IMREADME; Flags: fixed
Name: "DDX520X560I350"; Description: "{cm:DriversTitle}: X520, 560FLB, 560FLR, 560M, 562i, I350, M61KR-I"; Types: TTX520X560I350;
Name: "DDX540I350561T"; Description: "{cm:DriversTitle}: X540, I350, 561T, 561FLR"; Types: TTX540I350561T;
Name: "DDX550X562"; Description: "{cm:DriversTitle}: X550, 562T, 562FLR"; Types: TTX550X562;
Name: "DDX552"; Description: "{cm:DriversTitle}: X552"; Types: TTX552;
Name: "DDX553"; Description: "{cm:DriversTitle}: X553"; Types: TTX553;
Name: "DD82599X540VIRT"; Description: "{cm:VDriversTitle}: 82599, X540"; Types: TT82599X540VIRT;
Name: "DDX550X552X553VIRT"; Description: "{cm:VDriversTitle}: X550, X552, X553"; Types: TTX550X552X553VIRT;

[Files]
Source: "4.1.239.0\ixn68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DDX520X560I350; Flags:solidbreak;
Source: "4.1.239.0\ixn68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DDX520X560I350;
Source: "4.1.239.0\ixn68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DDX520X560I350;
Source: "4.1.239.0\ixn68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DDX520X560I350;
Source: "4.1.239.0\ixnmsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DDX520X560I350;

Source: "4.1.239.0\ixs68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DDX550X562; Flags:solidbreak;
Source: "4.1.239.0\ixs68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DDX550X562;
Source: "4.1.239.0\ixs68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DDX550X562;
Source: "4.1.239.0\ixs68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DDX550X562;
Source: "4.1.239.0\ixsmsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DDX550X562;

Source: "4.1.239.0\sxb68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DDX552; Flags:solidbreak;
Source: "4.1.239.0\sxb68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DDX552;
Source: "4.1.239.0\sxb68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DDX552;
Source: "4.1.239.0\sxb68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DDX552;
Source: "4.1.239.0\sxbmsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DDX552;

Source: "4.1.243.0\sxa68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DDX553; Flags:solidbreak;
Source: "4.1.243.0\sxa68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DDX553;
Source: "4.1.243.0\sxa68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DDX553;
Source: "4.1.243.0\sxa68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DDX553;
Source: "4.1.243.0\sxamsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DDX553;

Source: "4.1.228.0\ixt68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DDX540I350561T; Flags:solidbreak;
Source: "4.1.228.0\ixt68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DDX540I350561T;
Source: "4.1.228.0\ixt68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DDX540I350561T;
Source: "4.1.228.0\ixt68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DDX540I350561T;
Source: "4.1.228.0\ixtmsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DDX540I350561T;

Source: "2.1.241.0\vxn68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DD82599X540VIRT; Flags:solidbreak;
Source: "2.1.241.0\vxn68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DD82599X540VIRT;
Source: "2.1.241.0\vxn68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DD82599X540VIRT;
Source: "2.1.241.0\vxn68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DD82599X540VIRT;
Source: "2.1.241.0\vxnmsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DD82599X540VIRT;

Source: "2.1.230.0\vxs68x64.cat"; DestDir: "{tmp}\82599-driver"; Components: DDX550X552X553VIRT; Flags:solidbreak;
Source: "2.1.230.0\vxs68x64.din"; DestDir: "{tmp}\82599-driver"; Components: DDX550X552X553VIRT;
Source: "2.1.230.0\vxs68x64.inf"; DestDir: "{tmp}\82599-driver"; Components: DDX550X552X553VIRT;
Source: "2.1.230.0\vxs68x64.sys"; DestDir: "{tmp}\82599-driver"; Components: DDX550X552X553VIRT;
Source: "2.1.230.0\vxsmsg.dll"; DestDir: "{tmp}\82599-driver";   Components: DDX550X552X553VIRT;

Source: "intel-drivers-en.md"; DestName: "intel-82599-readme.txt"; DestDir: "{userdocs}"; Components: INTELREADME; Flags: isreadme onlyifdoesntexist; Languages: english;
Source: "intel-drivers-ru.md"; DestName: "intel-82599-readme.txt"; DestDir: "{userdocs}"; Components: INTELREADME; Flags: isreadme onlyifdoesntexist; Languages: russian;

[Messages]
WizardLicense=Agreement

[Run]
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\ixn68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:DriverTitle}: X520, 560XX, 562i, I350 ..."; \
	Description: "INTEL X520, 560XX, 562i, I350 64-bit {cm:DriverTitle} 4.1.239.0"; \
	Check: Is64BitInstallMode; \
	Components: DDX520X560I350;
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\ixs68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:DriverTitle}: X550, 562T, 562FLR ..."; \
	Description: "INTEL X550, 562T, 562FLR 64-bit {cm:DriverTitle} 4.1.239.0"; \
	Check: Is64BitInstallMode; \
	Components: DDX550X562;
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\sxb68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:DriverTitle}: X552 ..."; \
	Description: "INTEL X552 64-bit {cm:DriverTitle} 4.1.239.0"; \
	Check: Is64BitInstallMode; \
	Components: DDX552;
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\sxa68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:DriverTitle}: X553 ..."; \
	Description: "INTEL X553 64-bit {cm:DriverTitle} 4.1.243.0"; \
	Check: Is64BitInstallMode; \
	Components: DDX553;
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\ixt68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:DriverTitle}: X540, I350, 561T, 561FLR ..."; \
	Description: "INTEL X540, I350, 561T, 561FLR 64-bit {cm:DriverTitle} 4.1.228.0"; \
	Check: Is64BitInstallMode; \
	Components: DDX540I350561T;
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\vxn68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:VDriverTitle}: 82599, X540 ..."; \
	Description: "INTEL X540, 82599, X540 64-bit {cm:VDriverTitle} 2.1.241.0"; \
	Check: Is64BitInstallMode; \
	Components: DD82599X540VIRT;
Filename: "pnputil.exe"; Parameters: "/add-driver {tmp}\82599-driver\vxs68x64.inf /install"; \
	Flags: waituntilterminated runascurrentuser runhidden; \
	StatusMsg: "{cm:InstallTitle} INTEL {cm:VDriverTitle}: X550, X552, X553 ..."; \
	Description: "INTEL X550, X552, X553 64-bit {cm:VDriverTitle} 2.1.230.0"; \
	Check: Is64BitInstallMode; \
	Components: DDX550X552X553VIRT;

